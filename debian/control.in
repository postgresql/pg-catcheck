Source: pg-catcheck
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Michael Banck <michael.banck@credativ.de>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/postgresql/pg-catcheck
Vcs-Git: https://salsa.debian.org/postgresql/pg-catcheck.git
Homepage: https://github.com/EnterpriseDB/pg_catcheck/

Package: postgresql-PGVERSION-pg-catcheck
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: Postgres system catalog checker
 PostgreSQL stores the metadata for SQL objects such as tables and functions
 using special tables called system catalog tables. pg_catcheck is a simple
 tool for diagnosing system catalog corruption. If you suspect that your system
 catalogs are corrupted, this tool may help you figure out exactly what
 problems you have and how serious they are.  If you are paranoid, you can run
 it routinely to search for system catalog corruption that might otherwise go
 undetected.
